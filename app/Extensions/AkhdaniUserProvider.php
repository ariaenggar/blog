<?php

namespace App\Extensions;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;

class AkhdaniUserProvider implements UserProvider
{
    private function post(string $url, array $data){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $server_output = curl_exec($ch);

        curl_close ($ch);

        return $server_output;

    }

    private function get(string $url, array $params = []){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url . http_build_query($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        $server_output = curl_exec($ch);

        curl_close ($ch);

        return $server_output;

    }

    /**
     * @inheritDoc
     */
    public function retrieveById($identifier)
    {
        $result = $this->get("http://akhdani.net:12345/api/pegawai/$identifier");

        $obj = json_decode($result);
        $instance = new AkhdaniUser();
        $instance->set($obj);

        return $instance;
    }

    /**
     * @inheritDoc
     */
    public function retrieveByToken($identifier, $token)
    {
        $result = $this->get("http://akhdani.net:12345/api/pegawai/$identifier");

        $obj = json_decode($result);
        $instance = new AkhdaniUser();
        $instance->set($obj);

        return $instance;
    }

    /**
     * @inheritDoc
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        $user->setRememberToken($token);
    }

    /**
     * @inheritDoc
     */
    public function retrieveByCredentials(array $credentials)
    {
        $username = $credentials["username"];
        $result = $this->get("http://akhdani.net:12345/api/pegawai/username/$username");

        $obj = json_decode($result);
        $instance = new AkhdaniUser();
        $instance->set($obj);

        return $instance;
    }

    /**
     * @inheritDoc
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        $result = $this->post("http://akhdani.net:12345/api/auth/login", $credentials);

        return boolval($result);
    }
}
